import { ApiApplication } from './application';
import { ApplicationConfig } from '@loopback/core';
import { MargeDataSources } from './marge_data';

export { ApiApplication };

export async function main(options: ApplicationConfig = {}) {
  const app = new ApiApplication(options);
  const marge = new MargeDataSources()
  await marge.create('./src/charaters', 'data.json');
  await app.boot();
  await app.start();



  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);

  return app;
}
