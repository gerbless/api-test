import * as fs from 'fs';
import {Characters} from './models/';

export class MargeDataSources {
  async create(route: string, fileData: string) {
    const $readDirectory = this.readDirectory;
    const $readFile = this.readFile;
    const $writeFile = this.writeFile;
    const $slugify = this.slugify;
    fs.open(`${route}/${fileData}`, 'r', function(err) {
      if (err && err.code === 'ENOENT') {
        $readDirectory(route).then((file: string[]) => {
          $readFile(route, file)
            .then((data: string[]) => {
              const arr = data.map(r => {
                const obj: Characters = JSON.parse(r);
                obj.id = $slugify(obj.name);

                if (obj.id.indexOf('luke') === 0)
                  obj.id = obj.id.split('-', 1)[0];

                return obj;
              });

              const obj: object = {models: {Characters: arr}};

              $writeFile(route, obj, fileData)
                .then(r => console.log(r))
                .catch(error =>
                  console.log(`Typing error JSON: ${error}`),
                );
            })
            .catch(() => console.log(`Error reading data source`));
        });
      }
    });
  }

  writeFile(route: string, data: object, fileData: string) {
    const res = new Promise((resolve, reject) => {
      fs.writeFile(
        `${route}/${fileData}`,
        JSON.stringify(data),
        'utf8',
        err => {
          if (err) reject(err);

          resolve('The file JSON create saved!');
        },
      );
    });
    return res;
  }

  readFile(route: string, nameFile: string[]): Promise<any> {
    const res = new Promise((resolve, reject) => {
      const arr: string[] = [];
      nameFile.forEach((name: string, index: number) => {
        const text = fs.readFileSync(`${route}/${name}`, 'utf8');
        arr.push(text);

        if (nameFile.length === arr.length) resolve(arr);
      });
    });

    return res;
  }

  readDirectory(route: string): Promise<any> {
    const res = new Promise((resolve, reject) => {
      let filesList: string[] = [];
      fs.readdir(route, (err, files) => {
        if (err) reject(err);

        filesList = files.map(file => file);
        resolve(filesList);
      });
    });

    return res;
  }

  slugify(val: string) {
    return val
      .toString()
      .trim()
      .toLowerCase()
      .replace(/\s+/g, '-')
      .replace(/[^\w\-]+/g, '')
      .replace(/\-\-+/g, '-')
      .replace(/^-+/, '')
      .replace(/-+$/, '');
  }
}
