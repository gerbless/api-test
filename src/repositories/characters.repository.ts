import {DefaultCrudRepository} from '@loopback/repository';
import {Characters} from '../models';
import {DataDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class CharactersRepository extends DefaultCrudRepository<
  Characters,
  typeof Characters.prototype.id
> {
  constructor(
    @inject('datasources.data') dataSource: DataDataSource,
  ) {
    super(Characters, dataSource);
  }
}
