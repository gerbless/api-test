import {
  Filter,
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getFilterSchemaFor
} from '@loopback/rest';
import { Characters } from '../models';
import { CharactersRepository } from '../repositories';

export class CharactersController {
  constructor(
    @repository(CharactersRepository)
    public charactersRepository: CharactersRepository,
  ) { }

  @get('/characters', {
    responses: {
      '200': {
        description: 'Array of Characters model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Characters } },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Characters)) filter?: Filter<Characters>,
  ): Promise<Characters[]> {
    return await this.charactersRepository.find(filter);
  }

  @get('/character/{id}', {
    responses: {
      '200': {
        description: 'Characters model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Characters } } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Characters> {
    return await this.charactersRepository.findById(id);
  }

}
