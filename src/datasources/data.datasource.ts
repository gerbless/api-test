import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './data.datasource.json';

export class DataDataSource extends juggler.DataSource {
  static dataSourceName = 'data';

  constructor(
    @inject('datasources.config.data', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
