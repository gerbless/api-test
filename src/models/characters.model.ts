import { Entity, model, property } from '@loopback/repository';

@model({ settings: { strict: false } })
export class Characters extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id: string;

  @property({
    type: 'string',
  })
  name: string;

  @property({
    type: 'string',
  })
  height: string;

  @property({
    type: 'string',
  })
  mass?: string;

  @property({
    type: 'string',
  })
  hair_color?: string;

  @property({
    type: 'string',
  })
  skin_color?: string;

  @property({
    type: 'string',
  })
  eye_color?: string;

  @property({
    type: 'string',
  })
  birth_year?: string;

  @property({
    type: 'string',
  })
  gender?: string;

  @property({
    type: 'string',
  })
  homeworld?: string;

  @property({
    type: 'string',
  })
  image?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  species?: string[];

  @property({
    type: 'array',
    itemType: 'string',
  })
  vehicles?: string[];

  @property({
    type: 'array',
    itemType: 'string',
  })
  starships?: string[];

  @property({
    type: 'array',
    itemType: 'string',
  })
  films?: string[];


  constructor(data?: Partial<Characters>) {
    super(data);
  }
}
