## API characters Star Wares

Realizada con el framework loopback [![LoopBack](https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png)](http://loopback.io/)



## Instalación

1. npm install
2. npm start


## Repositorio

El API al iniciar  busca en la carpeta de repositorio si existe un archivo llamado data.json, en caso de no existir lee todos los archivos dentro del directorío src/charaters y crea el modelo de datos 
a partir del cotenido de los JSON encontrados.

## EndPoint
1. /character/{id}
2. /characters

Ademas puede aplicar filtros de busqueda como por ejemplo:
  
1. /characters?filter[where][name]=Darth Vader
2. /characters?filter[fields][id]=true


## Disponibilidad
El pipelines de esta app usa la metodogía de continus delivery. Por tanto es una app dokerizada lista para desplegar en kubernetes y está disponible en la dirección http://52.168.94.180:3000.
Si quieres hacer alguna actualización solo mandame un pull requests.


