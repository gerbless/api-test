FROM node:10.15.2-jessie

WORKDIR /usr/src/app

COPY package*.json ./
COPY ecosystem.config.js ./

RUN npm install -g pm2

RUN npm install

COPY . .

RUN npm run build

EXPOSE 3000
CMD [ "pm2-runtime", "ecosystem.config.js" ]

