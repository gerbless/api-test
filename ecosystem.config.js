module.exports = {
  apps: [
    {
      name: 'API',
      script: 'index.js',
      instances: 'max',
      exec_mode: 'cluster',
    },
  ],
};
